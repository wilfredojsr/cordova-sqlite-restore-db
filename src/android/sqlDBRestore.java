package me.wjsr.db.restore;

import java.io.File;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * This class echoes a string called from JavaScript.
 */
public class sqlDBRestore extends CordovaPlugin {

	public static String filepath = "filepath";
	private static final String TAG = "chromium";
	private CordovaInterface cordova = null;
	PluginResult plresult = new PluginResult(PluginResult.Status.NO_RESULT);

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
	    super.initialize(cordova, webView);
	    Log.v(TAG, "action=Plugin initialized");
	    this.cordova = cordova;
	}

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

		Log.v(TAG, "action=" + action);

		if (action.equalsIgnoreCase("restoreDB")) {

			Log.v(TAG, "filepath=" + args.getString(0));

			String db = args.getString(0);
			File path = cordova.getActivity().getDatabasePath(db);
			Boolean fileExists = path.exists();
			
			Log.v(TAG, "filepath exists=" + fileExists);

			if (fileExists) {
			
				boolean deleted = false;

				if (deleted) {

					plresult = new PluginResult(PluginResult.Status.OK, deleted);
					callbackContext.sendPluginResult(plresult);

				} else {

					plresult = new PluginResult(PluginResult.Status.ERROR,
							deleted);
					callbackContext.sendPluginResult(plresult);

				}
			
			} else {
			
				plresult = new PluginResult(PluginResult.Status.ERROR,
						"File Doesn't Exists oooooooooooo");
				callbackContext.sendPluginResult(plresult);
			
			}
			
			return true;
		
		} else {

			plresult = new PluginResult(PluginResult.Status.INVALID_ACTION);
			callbackContext.sendPluginResult(plresult);
		
			return false;
		
		}
	}

}
